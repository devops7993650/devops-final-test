from flask import Flask, render_template, request
from sklearn.linear_model import LinearRegression
import numpy as np
from random import randint


app = Flask(__name__)

X_train = np.array([1, 2, 3, 4, 5]).reshape(-1, 1)
y_train = np.array([2, 4, 5, 4, 5])

model = LinearRegression()
model.fit(X_train, y_train)

@app.errorhandler(Exception)
def fallback_solution(e):
    prediction = randint(-1000, 1000)
    return render_template('fallback.html', prediction=prediction)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        try:
            input_value = float(request.form['input_value'])
            prediction = model.predict(np.array([[input_value]]))[0]

            return render_template('index.html', prediction=prediction, input_value=input_value)
        except ValueError:
            return render_template('index.html', error="Please enter a valid number.")

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, port=8080)
